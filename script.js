fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => console.log(response.status))

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(response => console.log(response));


fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(data => {
    // Use the map method to create an array of just the title of every item
    const titles = data.map(item => item.title);
    console.log(titles);
  });

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((response) => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(response => {
    console.log(`The item "${response.title}" on the list has a status of ${response.completed}`);
  })
.catch(error => console.error('Error:', error));

fetch("https://jsonplaceholder.typicode.com/todos",
	{
		method: "POST",
		headers: 
			{"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			id: 1,
			title: "Created To Do List Item",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
	    	dateCompleted: "Pending",
		    description: "To update the my to do list with a different data structure",
		    id: 1,
		    status: "Pending",
		    title: "Updated To Do List Item",
	    	userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "07/09/2021",
			status: "Completed"
		})
	}

)
.then(response => response.json())
.then(response => console.log(response));


fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "DELETE"
	}
)
.then(response => response.json())
.then(response => console.log(response));